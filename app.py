# app.py

from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
import uuid
import hashlib
from bson.objectid import ObjectId

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'gigeliddb'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/gigeliddb'

mongo = PyMongo(app)

def hash_password(password):
    # uuid is used to generate a random number
    salt = 'antonganteng'
    return hashlib.sha256(salt.encode() + password.encode()).hexdigest()

def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()


@app.route("/")
def hello():
    return "Hello World!"

# [Auth]
@app.route('/auth', methods=['POST'])
def auth():
  user = mongo.db.users
  email = request.json['email']
  password = hash_password(request.json['password'])

  check_user = user.find_one({'email' : email, 'password': password})

  if check_user:
    output = {'status' : 'ok', 'result': {'_id' : str(check_user['_id']), 'name' : check_user['name'], 'email' : check_user['email'], 'phone' : check_user['phone']}}
  else:
    output = {'status' : 'notok', 'result': 'User not found'}
  return jsonify(output)


# [Users]
# Get all users
@app.route('/users', methods=['GET'])
def get_all_users():
  user = mongo.db.users
  output = []
  for u in user.find():
    output.append({'_id' : str(u['_id']), 'name' : u['name'], 'email' : u['email'], 'phone' : u['phone']})
  return jsonify({'status': 'ok', 'result' : output})

# Get one user
@app.route('/user/<field>/<value>', methods=['GET'])
def get_one_user(field, value):
  if (field == '_id'): 
    value = ObjectId(str(value))
  user = mongo.db.users
  u = user.find_one({field : value})
  if u:
    output = {'status' : 'ok', 'result': {'_id' : str(u['_id']), 'name' : u['name'], 'email' : u['email'], 'phone' : u['phone']}}
  else:
    output = {'status' : 'notok', 'result': 'User not found'}
  return jsonify(output)

# Add user
@app.route('/users', methods=['POST'])
def add_user():
  user = mongo.db.users
  name = request.json['name']
  email = request.json['email']
  password = hash_password(request.json['password'])
  phone = request.json['phone']

  check_email = user.find_one({'email' : email})
  check_phone = user.find_one({'phone' : phone})

  if check_email:
    output = {'status' : 'notok', 'result': 'Email '+email+' already exist!'}
  elif check_phone:
    output = {'status' : 'notok', 'result': 'Phone '+phone+' already exist!'}
  else:
    user_id = user.insert({'name': name, 'email': email, 'password': password, 'phone': phone})
    new_user = user.find_one({'_id': user_id })
    output = {'status' : 'ok', 'result': {'name' : new_user['name'], 'email' : new_user['email'], 'phone' : new_user['phone']}}
  return jsonify(output)

# Update user, name and phone number only
@app.route('/user/<id>', methods=['PATCH'])
def update_user(id):
  user = mongo.db.users
  name = request.json['name']
  phone = request.json['phone']

  check_user = user.find_one({'_id' : ObjectId(str(id))})
  check_phone = user.find_one({'phone' : phone})
 
  if check_user:
    if check_phone:
      if (check_phone['phone'] != check_user['phone']):
        output = {'status' : 'notok', 'result': 'Phone '+phone+' already exist!'}
      else:
        user_upd = user.find_and_modify({'_id':ObjectId(str(id))}, {'$set':{'name':name, 'phone': phone}})
        output = {'status' : 'ok', 'result': {'name' : name, 'email' : user_upd['email'], 'phone' : phone}}
    else:
      user_upd = user.find_and_modify({'_id':ObjectId(str(id))}, {'$set':{'name':name, 'phone': phone}})
      output = {'status' : 'ok', 'result': {'name' : name, 'email' : user_upd['email'], 'phone' : phone}}
  else:
    output = {'status' : 'notok', 'result': 'User not found'}
  return jsonify(output)

# Update password
@app.route('/user/changepwd/<id>', methods=['POST'])
def changepwd(id):
  user = mongo.db.users
  oldpassword = hash_password(request.json['oldpassword'])
  password1 = hash_password(request.json['password1'])
  password2 = hash_password(request.json['password2'])

  check_user = user.find_one({'_id' : ObjectId(str(id))})
 
  if check_user:
    if (oldpassword != check_user['password']):
      output = {'status' : 'notok', 'result': 'Wrong old password'}
    elif (password1 != password2):
      output = {'status' : 'notok', 'result': 'Password confirmation not matched'}
    else:
      user_upd = user.find_and_modify({'_id':ObjectId(str(id))}, {'$set':{'password':password1}})
      output = {'status' : 'ok', 'result': {'name' : user_upd['name'], 'email' : user_upd['email'], 'phone' : user_upd['phone']}}
  else:
    output = {'status' : 'notok', 'result': 'User not found'}
  return jsonify(output)

if __name__ == '__main__':
    app.run(debug=True)